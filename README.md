### Get started

This repository is for experimenting with the gitlab runners. The helm charts are available here: https://gitlab.cern.ch/rbritoda/metar/-/tree/master/services/gitlab-runner

We will deploy 2 gitlab runners:
* The default runner, it can schedule 5 jobs concurently. The jobs are scheduled with priority -10 (default is 0). This jobs will be executed only when there is no load from batch or ML.
* The high priority runner, it will make sure the high priority jobs are executed first.

First, create the priority classes: ```kubectl apply -f priority_classes.yaml```

Get the ```runnerRegistrationToken``` from gitlab and add it to the values.yaml file.

Use the charts mentioned above, with the values from this repository:

```helm install -n gitlab gitlab-runner -f values.yaml .```

```helm install -n gitlab gitlab-runner-high-priority -f values.yaml -f values_high_priority.yaml .```


### GPU usage

Gitlab Runners do not provide a way to configure the requirement for GPU.

Until this functionality is added by gitlab, we can use the mutating webhook available here: https://github.com/rochaporto/gitlab-runner-resources-webhook

It will add the ```requires``` and ```limits``` fields on creation of the job pod. More info on how to start the webhook and what issues might appear: https://gitlab.cern.ch/kubernetes/project/-/issues/404

